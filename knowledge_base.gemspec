$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "knowledge_base/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "knowledge_base"
  s.version     = KnowledgeBase::VERSION
  s.authors     = ["Matt Young"]
  s.email       = ["mcyoung86@gmail.com"]
  s.summary     = "Summary of KnowledgeBase."
  s.description = "Description of KnowledgeBase."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "3.2.22"
  s.add_dependency "sprockets-rails"
  s.add_dependency "strong_parameters"
  s.add_dependency 'haml'
  s.add_dependency "jquery-rails"
  
  s.add_dependency 'bootstrap-sass', '3.3.4.1'
  s.add_dependency 'sass-rails', '3.2.6'

  s.add_dependency 'ckeditor'

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'factory_bot_rails'
end
