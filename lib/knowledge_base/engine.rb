module KnowledgeBase
  class Engine < ::Rails::Engine
    isolate_namespace KnowledgeBase
    # => Migrations
    # => This has to be kept in an initializer (to access app)
    # => https://blog.pivotal.io/labs/labs/leave-your-migrations-in-your-rails-engines
    initializer :migration_paths do |app|
      config.paths["db/migrate"].expanded.each do |expanded_path|
        app.config.paths["db/migrate"] << expanded_path
      end
    end
  end
end
